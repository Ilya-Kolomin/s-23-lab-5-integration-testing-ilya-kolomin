# Lab5 -- Integration testing


# Tables

https://docs.google.com/spreadsheets/d/1USs9vaOXVvD7z1b2quV9GgATVuvKq06ArA-krEr2hUs/edit?usp=sharing

# Found bugs

(By "empty" I mean that this parameter is not present in URL)

1. Domain error: parameter `distance` should not be allowed to be empty for fixed plan.
2. Domain error: parameter `planned_distance` should not be allowed to be empty for fixed plan.
3. Domain error: parameter `time` should not be allowed to be empty for fixed plan.
4. Domain error: parameter `planned_time` should not be allowed to be empty for fixed plan.
5. Domain error: parameter `time` should not be allowed to be empty for minute plan.
6. Domain error: parameter `distance` should not be allowed to be non-empty for minute plan.
7. Domain error: parameter `planned_distance` should not be allowed to be non-empty for minute plan.
8. Domain error: parameter `planned_time` should not be allowed to be non-empty for minute plan.
9. Domain error: parameter `type` should not be allowed to be `luxury` for fixed plan.
10. Computation error: in all variations the innopolis discounts is computed uncorrectly.
11. Computation error: in fixed plan case, the price is computed incorrectly.
12. Computation error: in fixed plan case, when the deviation exceeded, the price is computed incorrectly.